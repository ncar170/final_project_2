package POJO;

import java.util.Calendar;
import java.util.Date;

public class ArticlePOJO {
    private int article_id;
    private String title;
    private String content;
    private int day;
    private int month;
    private int year;
    private Calendar date2;
    private String date;
    private int user_id;

    public ArticlePOJO(){

    }

    public int getArticle_id() {
        return article_id;
    }

    public void setArticle_id(int article_id) {
        this.article_id = article_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) { this.title = title; }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Calendar getDate2() {
        return date2;
    }

    public void setArticleDate(int day, int month, int year) {
        date2.set(year, month, day);
    }

    public void setDate2(Calendar date2) {
        this.date2 = date2;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
